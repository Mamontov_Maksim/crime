package com.example.pj.criminalintent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Crime {

    private UUID mId;
    private String mTitle;
    private Date mDate;
    private boolean mSolved;
    private String mSuspect;

    private class MyDate extends Date {
        @Override
        public String toString() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
            return simpleDateFormat.format(mDate);
        }
    }


    public Crime (){
        this(UUID.randomUUID());
    }

    public Crime(UUID uuid){
        mId = uuid;
        mDate = new Date();
    }

    public Date getDate() {
        return new MyDate();
    }

    public String getTitle() {
        return mTitle;
    }

    public UUID getId() {
        return mId;
    }

    public boolean isSolved() {
        return mSolved;
    }

    public void setDate(Date mDate) {
        this.mDate = mDate;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setSolved(boolean mSolved) {
        this.mSolved = mSolved;
    }

    public String getSuspect() {
        return mSuspect;
    }

    public void setSuspect(String mSuspect) {
        this.mSuspect = mSuspect;
    }

    public String getPhotoFilename(){
        return "IMG_" + getId().toString() + ".jpg";
    }
}
