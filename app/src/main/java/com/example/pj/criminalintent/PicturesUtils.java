package com.example.pj.criminalintent;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;

public class PicturesUtils {

    public static Bitmap getScaledBitmap (String path, int destWeight, int destHeight){
        // Чтение размеров изображения на диске
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        float srcWidth = options.outWidth;
        float srcHeight = options.outHeight;

        // Вычесление стпени маштабирования
        int inSampleSize = 1;
        if (srcHeight > destHeight || srcWidth > destWeight){
            float heightScale = srcHeight/destHeight;
            float widthScale = srcWidth/destWeight;

            inSampleSize = Math.round(heightScale > widthScale ? heightScale : widthScale);

        }

        options = new BitmapFactory.Options();
        options.inSampleSize = inSampleSize;

        //Чтения файлов и создание итогового изображения
        return BitmapFactory.decodeFile(path, options);
    }

    public static Bitmap getScaledBitmap(String path, Activity activity){
        Point point = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(point);

        return getScaledBitmap(path, point.x, point.y);
    }
}
